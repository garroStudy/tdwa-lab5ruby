Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/todos', to: 'todos#all'
  post '/addTodo', to: 'todos#create'
  post '/removeTodo', to: 'todos#delete'
  post '/doneTodo', to: 'todos#done'
  post '/failTodo', to: 'todos#fail'
end
