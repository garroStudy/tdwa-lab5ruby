class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.string :title
      t.string :text
      t.string :status
      t.datetime :date
      t.timestamps
    end
  end
end
