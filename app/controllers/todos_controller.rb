class TodosController < ApplicationController

  def all
    render json: TodoSerializer.new(Todo.all).serialized_json
  end

  def create
    todo = Todo.new(todo_params)
    todo.status = 'in progress'
    todo.save
    render json: TodoSerializer.new(todo).serialized_json
  end

  def delete
    todo = Todo.find(params[:id]).destroy
    render json: TodoSerializer.new(Todo.all).serialized_json
  end

  def done
    Todo.find(params[:id]).update(status: 'done', date: Time.now.httpdate)
    todo = Todo.find(params[:id])
    render json: TodoSerializer.new(todo).serialized_json
  end

  def fail
    Todo.find(params[:id]).update(status: 'fail', date: Time.now.httpdate)
    todo = Todo.find(params[:id])
    render json: TodoSerializer.new(todo).serialized_json
  end

  def removeIdParse
    params.permit(:id)
  end

  def todo_params
    params.permit(:title, :text)
  end

end
