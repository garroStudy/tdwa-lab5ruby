class TodoSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :text, :status, :date
end
